import { LightningElement, track, api } from 'lwc';
import deleteContact from '@salesforce/apex/AccConCnt.deleteContact';

export default class ContactCmp extends LightningElement {
    @api isContact;
    @api arrContact;
    @api uuidCont;
    @api contactId;
    @api accountId;
    @api contactItem
    @track showSpinner=false;
    @track contact={
        Id:null,
        UUIDCont__c:"",
        FirstName:"",
        LastName:"",
        Birthdate:null,
        Phone:"",
        Email:"",
        Fax:"",
        FiscalCode__c:"",
        PreferedContact__c:"",
        AccountId:null
    }

    connectedCallback() {
        // Se this.contactItem è valorizzato...
        if(this.contactItem != undefined && this.contactItem != null) {
          // ...assegno all'oggetto contact (this.contact) un singolo oggetto contact (this.contactItem)
          this.contact = this.contactItem;
        }
      }

    @api getContactValues(event){
        let arrField=this.template.querySelectorAll("lightning-input-field");
        let cont={
        UUIDCont__c:this.uuidCont,
        Id:null,
        AccountId:null
        };
        if (this.contactId){
            cont.Id=this.contactId;
            cont.AccountId=this.accountId;
        }
        arrField.forEach(elem => {
            cont[elem.fieldName]=elem.value;
        });
        this.contact=cont;
        return this.contact;
    
    }

    @api validateContact(event){
        var errorContact = {
            status: true,
            error: ""
        };
        const inputFields = this.template.querySelectorAll('.validate');
        inputFields.forEach((elem) => {
            if (elem.value == undefined || elem.value == null || elem.value == "") {
                errorContact.status = false;
                errorContact.error+=elem.fieldName+" ";
            }
        });
        return errorContact;
    }

    handleDelete(event) {
        this.showSpinner=true;
        let index = this.arrContact.findIndex(elem => elem.UUIDCont__c === this.uuidCont);
        if (this.contactId){
            deleteContact({contactId:this.contactId}).then(response => {
                this.showSpinner=false;
            }).catch(error => {
                console.log(error.getMessage());
                this.showSpinner=false;
            })
        }
        this.showSpinner=false;
        let contactId=this.contactId;
        const tileClicked = new CustomEvent("delete", { detail: {index: index, contactId:contactId} });
        this.dispatchEvent(tileClicked);
    }
}