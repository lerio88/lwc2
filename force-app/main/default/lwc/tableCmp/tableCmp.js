import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
export default class TableCmp extends NavigationMixin(LightningElement) {
    @api accValues;
    @api displayList;
    @track actions = [
        { label: 'Edit', name: 'edit' },
    ];
    @track columns = [
        { label: 'Name', fieldName: 'Name' },
        { label: 'Business Type', fieldName: 'BusinessType__c' },
        { label: 'Activation Date', fieldName: 'ActivationDate__c' },
        { label: 'City', fieldName: 'BillingCity' },
        {
            type: 'action',
            typeAttributes: {
                rowActions: this.actions,
                menuAlignment: 'right'
            }
        }
    ];
    handleRowActions(event) {
        let row = event.detail.row;
        let accountId=this.viewCurrentRecord(row);
        this.navigateToFormAcc(accountId);
        //Passa l'account con la naviagazione
    }

    viewCurrentRecord(currentRow){
        console.log(currentRow.Id);
        return currentRow.Id;
    }

    navigateToFormAcc(accountId) {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            type: "standard__navItemPage",
            attributes: {
                apiName: 'lwc2',
            },
            state: {
                c__accountId: accountId
            }
        });
    }
    }