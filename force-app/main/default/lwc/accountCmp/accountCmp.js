import { LightningElement, track, api } from 'lwc';

export default class AccountCmp extends LightningElement {
    @api accountId;
    @api accountValues;
    @track account = {
        Id: null,
        Name: "",
        Privacy__c: "",
        BusinessType__c: "",
        BillingStreet: "",
        BillingCountry: "",
        ActivationDate: null
    }

    @api getAccountValues(event) {
        let arrField = this.template.querySelectorAll("lightning-input-field");
        let acc = {
            Id: null
        };
        if (this.accountId) {
            acc.Id = this.accountId;
        }
        arrField.forEach(elem => {
            acc[elem.fieldName] = elem.value;
        });
        this.account = acc;
        return this.account;
    }

    @api setAccountValues(accValues){
        this.account=accValues;
        console.log("ACCOUNT TORNATO :"+ JSON.stringify(this.account));
    }

    @api
    validateAccount(event) {
        var errorAccount = {
            status: true,
            error: ""
        };
        const inputFields = this.template.querySelectorAll('.validate');
        inputFields.forEach((elem) => {
            if (elem.value == undefined || elem.value == null || elem.value == "") {
                errorAccount.status = false;
                errorAccount.error += elem.fieldName + " ";
            }
        });
        return errorAccount;
    }
}