import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveAll from '@salesforce/apex/AccConCnt.saveAll';
import getAccCont from '@salesforce/apex/AccConCnt.getAccCont';
export default class WrapperCmp extends LightningElement {
    @track contacts = [
    ];
    @track accountId;
    @track count = 0;
    @track showSpinner=false;

    connectedCallback(){
        let url= document.URL;
        let accIdStr=url.indexOf("=");
        if (accIdStr!=-1){
            let accId=url.substring(accIdStr+1, url.length);
            getAccCont({ id: accId }).then(response => {
                let accountVal = response.content.account;
                let contacts = response.content.contacts;
                let accMet = this.template.querySelector("c-account-cmp");
                accMet.setAccountValues(accountVal);
                if (contacts.length != 0){
                this.contacts=contacts;
                }
                console.log("RESPONSE CONTACT: " + JSON.stringify(this.contacts));
                this.showSpinner=false;
            }).catch(error => {
                console.log(error.getMessage());
                this.showToastAccount("Error", error.getMessage(), "error");
                this.showSpinner=false;
            })
        }
    }
    newContact(event) {
        this.count++;
        let contact = {
            Id: null,
            UUIDCont__c: "",
            FirstName: "",
            LastName: "",
            Birthdate: "",
            Phone: "",
            Email: "",
            Fax: "",
            FiscalCode__c: "",
            PreferedContact__c: ""
        };
        contact.UUIDCont__c = this.count;
        this.contacts.push(contact);
    }

    handleDelete(event) {
        const arrCont = event.detail;
        let index = arrCont.index;
        let contactId= arrCont.contactId;
        this.contacts.splice(index, 1);
        this.showToastAccount("Delete", "Contact "+contactId+" deleted", "error");
    }

    handleSave(event) {
        this.showSpinner=true;
        let accMet = this.template.querySelector("c-account-cmp");
        let conMet = this.template.querySelectorAll("c-contact-cmp");
        //TODO VALIDAZIONE
        let boolValAcc = false;
        let boolValCon = false;
        let accError="";
        let conError="";
        let validateAccount=accMet.validateAccount();
        if (validateAccount.status) {
            //valida contact se validazione account true
            let count=0;
            boolValAcc = true;
            conMet.forEach(elem => {
                if (elem.validateContact().status){
                    boolValCon = true;
                }
                else {
                    count++;
                    boolValAcc = false;
                    boolValCon = false;
                    conError= elem.validateContact().error;
                    this.showToastAccount("Validated fields", "Contact "+count +". Compilare " +conError, "Warning");
                }
            });
        }

        if(validateAccount.status==false){
            boolValAcc = false;
            boolValCon = false;
            accError=validateAccount.error;
            this.showToastAccount("Validated fields", "Compilare "+accError, "Warning");
        }
        
        if (boolValAcc || boolValCon) {
            //GET ACCOUNT VALUES
            let accValues = accMet.getAccountValues();
            //GET CONTACT VALUES
            let conValues = [];
            conMet.forEach(elem => {
                conValues.push(elem.getContactValues());
            });
            this.contacts = conValues;
            //CALL APEX METHOD
            let inputs = {
                account: JSON.stringify(accValues),
                contacts: JSON.stringify(this.contacts)
            };
            console.log("ACCOUNT :" + JSON.stringify(accValues));
            console.log("CONTACT :" + JSON.stringify(this.contacts));
            saveAll({ params: inputs }).then(response => {
                this.contacts = response.content.contacts;
                this.accountId = response.content.account.Id;
                console.log("RESPONSE CONTACT: " + JSON.stringify(this.contacts));
                this.showToastAccount("Success", "Records Inserted", "success");
                this.showSpinner=false;
            }).catch(error => {
                console.log(error.getMessage());
                this.showToastAccount("Error", error.getMessage(), "error");
                this.showSpinner=false;
            })
        }
        this.showSpinner=false;
    }

    // Toast di errore dei campi non compilati del formAccount
  showToastAccount(title, message, variant) {
    const event = new ShowToastEvent({
      title: title,
      message: message,
      variant: variant
    });
    this.dispatchEvent(event);
  }
}