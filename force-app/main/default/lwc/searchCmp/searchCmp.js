import { LightningElement, track, api } from 'lwc';
import getAccountSearch from '@salesforce/apex/AccConCnt.getAccountSearch';

export default class SearchCmp extends LightningElement {
    @track accValues;
    connectedCallback(event) {
        this.handleList(event);
    }

    handleList(event) {
        let accountName = "";
        let city = "";
        getAccountSearch({ accountName: accountName, city: city }).then(response => {
            console.log("RESPONSE CONTACT: " + JSON.stringify(response));
            this.accValues = response.content.accounts;
        }).catch(error => {
            console.log(error.getMessage());
        })
    }

    handleChange(event) {
        let eventName = event.target.name;
        let eventValue = event.target.value;
        let name= this.template.querySelector('lightning-input[data-id="AccountName"]').value;
        let city= this.template.querySelector('lightning-input[data-id="Country"]').value;
        if (eventValue.length >= 3) {
            if (eventName === "AccountName") {
              name = eventValue;
            } else if (eventName === "Country") {
              city = eventValue;
            }
            getAccountSearch({ accountName: name, city: city }).then(response => {
                console.log("RESPONSE CONTACT: " + JSON.stringify(response));
                this.accValues = response.content.accounts;
            }).catch(error => {
                console.log(error.getMessage());
            })
            //FAI PARTIRE APEX
        }
        else {
            this.handleList(event);
        }
    }
}