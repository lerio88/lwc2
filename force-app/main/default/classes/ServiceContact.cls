public with sharing class ServiceContact {
    private static ContactRepository contactRepository = new ContactRepository();
    // Metodo che esegue dei controlli per l'insert e l'update dell'account e dei contatti
    public List<Contact> searchContacts(List<Contact> contactData, String accountId) {
        List<Contact> listContacts = new List<Contact>();
        // Se la lista dei contatti non è vuota
        if(contactData.size() != 0) { 
            // Allora ciclo la lista
            for(Contact cnt : contactData) {
                // Se l'Id non esiste
                if(cnt.Id == null) {
                    // Associo l'Id dell'account ai contatti
                    cnt.AccountId = accountId;
                }
            }
        }
        // Fuori dal ciclo faccio l'insert o l'update (upsert) 
        listContacts = contactRepository.upsertContact(contactData);
        return listContacts;
    }

    public static Map<String, Object> deleteContact(String contactId){
        Map<String, Object> contentResponse = new Map<String, Object>();
        String accountId=contactRepository.deleteContact(contactId);
        contentResponse.put('accountId', accountId);
        return contentResponse;
    }
}
