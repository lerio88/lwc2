public with sharing class AccountRepository {
    public Account insertAccount(Account accountData) {
        insert accountData;
        return accountData;
    }
    
    // Update Account
    public Account updateAccount(Account accountData) {
        update accountData;
        return accountData;
    }

    public List<Account> getQryAccount(String accountName, String city){
        String acc= '%'+accountName+'%';
        String ct= '%'+city+'%';
        return [SELECT Id, Name, BusinessType__c, BillingCity, ActivationDate__c FROM Account WHERE Name LIKE :acc AND BillingCity LIKE :ct];
    }

    public Account getQryIdAccount(String id){
        return [SELECT Id, Name, Privacy__c, BusinessType__c, BillingStreet, BillingCity, ActivationDate__c FROM Account WHERE Id=:id];
    }
}
