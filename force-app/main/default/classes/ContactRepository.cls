public with sharing class ContactRepository {
    public List<Contact> upsertContact(List<Contact> contactData) {
        upsert contactData;
        return contactData;
    }

    public String deleteContact(String contactId){
        Contact con= [SELECT Id FROM Contact WHERE Id=:contactId];
        delete con;
        return con.Id;
    }

    public List<Contact> getQryIdContact(String id){
        return [SELECT Id, AccountId, FirstName, LastName, Birthdate, Phone, Email, Fax, FiscalCode__c, PreferedContact__c FROM Contact WHERE AccountId=:id];
    }
    }
