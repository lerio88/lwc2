public with sharing class AccConCnt {
        private static DTOResponse response = new DTOResponse();
        private static ServiceAccount accountService = new ServiceAccount();
        private static ServiceContact contactService = new ServiceContact();

    @AuraEnabled
    public static DTOResponse saveAll(Map<String,String> params){
        //Map<String,Object> response= New Map<String,Object>();
        try{
            Account accountData = (Account)JSON.deserialize(params.get('account'), Account.class);
            System.debug('accountData: ' + accountData);
            List<Contact> contactData = (List<Contact>)JSON.deserialize(params.get('contacts'), List<Contact>.class);
            System.debug('contactData: ' + contactData);
            response.content = accountService.searchAccountAndContacts(accountData, contactData);
            response.status=true;
        } catch(Exception e){
            response.status=false;
            response.message=e.getMessage();
        }
        return response;
    }

    @AuraEnabled
    public static DTOResponse deleteContact(String contactId){
        try{
            response.content = ServiceContact.deleteContact(contactId);
            response.status=true;
        } catch(Exception e){
            response.message=e.getMessage();
            response.status=false;
        }
        return response;
    }

    @AuraEnabled
    public static DTOResponse getAccountSearch(String accountName, String city){
        try{
            response.status=true;
            response.content= accountService.getAccountSearch(accountName, city);
        } catch(Exception e){
            response.message=e.getMessage();
            response.status=false;
        }
        return response;
    }

    @AuraEnabled
    public static DTOResponse getAccCont(String id){
        //Map<String,Object> response= New Map<String,Object>();
        try{
            response.status=true;
            response.content= accountService.getAccContact(id);
        } catch(Exception e){
            response.message=e.getMessage();
            response.status=false;
        }
        return response;
    }
}
