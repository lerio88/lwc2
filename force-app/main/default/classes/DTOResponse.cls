public with sharing class DTOResponse {
    @AuraEnabled
	public Map<String, Object> content = new Map<String, Object>();
    @AuraEnabled
    public String message = '';
    @AuraEnabled
    public Boolean status = false;

    public Map<String, Object> getContent() {
        return content;
      }
    
      public void setContent(Map<String, Object> content) {
        this.content = content;
      }

      public String getMessage() {
        return message;
      }
    
      public void setMessage(String message) {
        this.message = message;
      }

      public Boolean getStatus() {
        return status;
      }
    
      public void setStatus(String message) {
        this.status = status;
      }
}
