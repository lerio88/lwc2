//in questa classe sono impostati i metodi per l'accesso ai dati dell'applicazione
public with sharing class ServiceAccount {
    private static AccountRepository accountRepository = new AccountRepository();
    private static ContactRepository contactRepository = new ContactRepository();
    private static ServiceContact serviceContact = new ServiceContact();
    // Metodo che esegue dei controlli per l'insert e l'update dell'account e dei contatti
    public Map<String, Object> searchAccountAndContacts(Account accountData, List<Contact> contactData) {
        Map<String, Object> contentResponse = new Map<String, Object>();
        Account contentAccount;
        List<Contact> contentContacts = new List<Contact>();
        contentAccount = searchAccount(accountData);
        contentContacts = serviceContact.searchContacts(contactData, contentAccount.Id);
        contentResponse.put('account', contentAccount);
        contentResponse.put('contacts', contentContacts);
        return contentResponse;
    }

    public Account searchAccount(Account accountData) {
        Account contentAccount;
        // Controllo sulle query dell'account
        // Se il form dell'account non ha l'Id allora faccio l'insert
        if(accountData.Id == null) {
            contentAccount = accountRepository.insertAccount(accountData);
        } else {
            // Altrimenti se ho già l'Id faccio l'update
            contentAccount = accountRepository.updateAccount(accountData);
        }
        return contentAccount;
    }

    public Map<String, Object> getAccountSearch(String accountName, String city) {
        Map<String, Object> contentResponse = new Map<String, Object>();
            List<Account> contentAccount;
            contentAccount = accountRepository.getQryAccount(accountName, city);
            contentResponse.put('accounts', contentAccount);
            return contentResponse;
        }

        public Map<String, Object> getAccContact(String id){
            Map<String, Object> contentResponse = new Map<String, Object>();
            Account contentAccount;
            List<Contact> contactAccount;
            contentAccount = accountRepository.getQryIdAccount(id);
            contactAccount = contactRepository.getQryIdContact(id);
            contentResponse.put('account', contentAccount);
            contentResponse.put('contacts', contactAccount);
            return contentResponse;
        }
}
